#! /usr/bin/env node
const chalk = require("chalk");
const clear = require("clear");
const app = require("commander");
const figlet = require("figlet");

const clone = require("./clone_repo");
const patch = require("./create_patch");

app
  .command("init")
  .description("Run CLI tool")
  .action(async () => {
    clear();
    console.log(
      chalk.magentaBright(
        figlet.textSync("GIT Commands", { horizontalLayout: "fitted" })
      )
    );

    await clone.cloneRepo().then(async (remoteUrl) => {
      await patch.createPatch(remoteUrl);
    });
  });

app.parse(process.argv);

if (!app.args.length) {
  app.help();
}
