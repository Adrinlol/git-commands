## git-commands

CLI for automating git commands. Used for cloning, creating branches, patching, committing, and pushing.

<img src="https://user-images.githubusercontent.com/48876996/214873515-bded765a-58ce-42de-9179-099256e867ad.png" alt="git-commands" width="900" height="auto"/>

## Install

```sh
npm i @adrinlol/git-commands
```

or if you use yarn

```sh
yarn @adrinlol/git-commands

```

## Usage

```sh
git-commands init

```
