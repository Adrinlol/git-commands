const fs = require("fs");
const path = require("path");
const https = require("https");
const chalk = require("chalk");
const figlet = require("figlet");
const git = require("simple-git")();
const inquirer = require("inquirer");

async function createPatch(remoteUrl) {
  const repoName = () => remoteUrl.split("/").pop().replace(".git", "");
  const directoryPath = path.join(process.cwd(), `./${repoName()}`);

  process.chdir(directoryPath);

  const question = [
    {
      name: "name",
      type: "input",
      message: "Enter a new branch name",
      default: path.basename(process.cwd()),
      validate: function (value) {
        if (value.length) {
          return true;
        } else {
          return "Please enter a valid input.";
        }
      },
    },
  ];

  const branchAnswer = await inquirer.prompt(question);

  try {
    await git
      .cwd(directoryPath)
      .branch(branchAnswer.name)
      .checkoutLocalBranch(branchAnswer.name)
      .push(remoteUrl, branchAnswer.name)
      .then(async () => {
        const question = [
          {
            name: "name",
            type: "input",
            message: "Enter patch file url",
            default: path.basename(process.cwd()),
            validate: function (value) {
              if (value.length) {
                return true;
              } else {
                return "Please enter a valid input.";
              }
            },
          },
        ];

        const patchFileAnswer = await inquirer.prompt(question);

        https.get(patchFileAnswer.name, (res) => {
          const fileStream = fs.createWriteStream(
            path.basename(patchFileAnswer.name)
          );
          res.pipe(fileStream);

          fileStream.on("finish", () => {
            const directoryName = () =>
              remoteUrl.split("/").pop().replace(".git", "");

            const patch = path.join(
              process.cwd(),
              `../${directoryName()}/`,
              path.basename(patchFileAnswer.name)
            );

            git.applyPatch(patch).then(async () => {
              fs.rmSync(patch);
              await git
                .add("./*")
                .commit("Apply patch")
                .push(remoteUrl, branchAnswer.name, ["--set-upstream"])
                .then(() => {
                  console.log(
                    chalk.magentaBright(
                      figlet.textSync("Success", {
                        horizontalLayout: "controlled smushing",
                      })
                    )
                  );
                });
            });
          });
        });
      });
  } catch (e) {
    console.log("Something went wrong during branch creation", e);
  }
}

module.exports = { createPatch };
