const fs = require("fs");
const path = require("path");
const git = require("simple-git")();
const inquirer = require("inquirer");

async function cloneRepo() {
  const question = [
    {
      name: "name",
      type: "input",
      message: "Enter a repository name.",
      default: path.basename(process.cwd()),
      validate: function (value) {
        if (value.length) {
          return true;
        } else {
          return "Please enter a valid input.";
        }
      },
    },
  ];

  const answers = await inquirer.prompt(question);
  const repoName = () => answers.name.split("/").pop().replace(".git", "");

  if (fs.existsSync(repoName())) {
    try {
      fs.rmSync(repoName(), { recursive: true });
    } catch (e) {
      console.log("Something went wrong during rmSync", e);
    }
  }

  try {
    await git.clone(answers.name);
  } catch (e) {
    console.log("Something went wrong during clone", e);
  }
  return answers.name;
}

module.exports = { cloneRepo };
